console.log('page load - entered main.js for js-other api');
alert("Make sure you run Stock REST API server!");

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("name-text").value;
    console.log('Stock you entered is ' + name);
    makeNetworkCallToAgeApi(name);

} // end of get form info

function makeNetworkCallToAgeApi(name){
    console.log('entered make nw call' + name);
    alert("Making 1st API call!");
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://localhost:51056/stockinfo/" + name;
    console.log(url)
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateAgeWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateAgeWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");
    console.log('Closing Price is : ', +response_json['ClosingPrice'])
    if(response_json['ClosingPrice'] == null){
        label1.innerHTML = 'Apologies, we could not find the desired stock.'
    } else{
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        newdate = month + "/" + day + "/" + year;
        label1.innerHTML =  name + ', ClosingPrice for ' +newdate+ ' is ' + response_json['ClosingPrice'];
        callSecondApi(name);
    }
} // end of updateAgeWithResponse


function callSecondApi(name){
    console.log('This is External API call');
    alert("Making 2nd API call!");
    // set up url
    var xhr = new XMLHttpRequest(); 

    var url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" +name+ "&interval=5min&apikey=3F3AGB6X5ZLT7EKM";
    console.log("This is the call url you are making : " +url)
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        var abc = xhr.responseText;  
        abc = JSON.parse(abc);
        abc = abc['Meta Data']
        console.log(abc);
        // do something
        updateStockWithResponse(name, abc);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateStockWithResponse(name, abc){
    console.log('This is response_json: ' +abc);

    var label2 = document.getElementById("response-line2");

    if(abc == null){
        label2.innerHTML = 'Apologies, we could not find the desired stock Information.'
    } else{
        label2.innerHTML =  'This is the Information we could find about ' +name+ ' : ' + abc['1. Information'];
    }
} // end of updateAgeWithResponse
