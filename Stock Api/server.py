import routes
import cherrypy
from stock_controller import StockController

def start_service():
    #TODO create a controller obj
    d_cont = StockController()

    #TODO setup dispacher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # connect resources endpoints using dispacher

    #put_key
    dispatcher.connect('dict_put_key', '/stockinfo/:key', controller= d_cont, action='GET_StockInfo', 
    conditions=dict(method=['GET']) )

    #post
    dispatcher.connect('dict_post_method', '/stockdetailedinfo/:key', controller= d_cont, action='GET_StockDetailedInfo', 
    conditions=dict(method=['GET']) )

    #Get_key
    dispatcher.connect('dict_get_key', '/stockpredictions/:key', controller= d_cont, action='GET_StockPrediction', 
    conditions=dict(method=['GET']) )


    dispatcher.connect('dict_options', '/dictionary/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('dict_key_options', '/dictionary/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    # configure server
    conf = {
        'global': {
            'server.socket_host': 'localhost', #student04.cse.nd.edu
            'server.socket_port' : 51056
        },
        '/': {
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True, # configuration for CORS
        }
    }

    #update conf with cherrypy
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config = conf)

    #TODO start server
    cherrypy.quickstart(app)

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
    start_service()