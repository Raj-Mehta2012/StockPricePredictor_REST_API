import json
from datetime import date

a = ['a','b','c','d']

today = date.today()
d1 = today.strftime("%m/%d/%Y")
print("d1 =", d1)

output = {'result' : 'success'}
output['value'] = a
print(json.dumps(output))